;;; evilre.el --- remap EVIL keybindings to put most-used movement keys on the home row  -*- lexical-binding: t; -*-

;; currently only Colemak keyboard mapping is implemented

;; Copyright (C) 2016 Bayle Shanks

;; Author: Bayle Shanks
;; Keywords: evil

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; developed with EVIL mode 1.2.12 (see https://bitbucket.org/lyro/evil/wiki/Home and https://www.emacswiki.org/emacs/Evil )

;; must be called after loading EVIL
;;


;;; Code:

;; TODO:
;;    CUT (xt) and COPY (ct) should not go to end of line
;;    consecutive xx should add to the cut text, not replace it
;;    mark commands

(require 'cl)
(defun evilre-construct-viper-goto-registered-mark-keybindings ()
  (let ((letters '(?a ?b ?c ?d ?e ?f ?g ?h ?i ?j ?k ?l ?m ?n ?o ?p ?q ?r ?s ?t ?u ?v ?w ?x ?y ?z)) (bindings (make-sparse-keymap)))
  (dolist (letter letters bindings) 
  (define-key bindings (vector letter) (lexical-let ((letter letter)) (lambda (arg) (interactive "P") (funcall (apply-partially 'viper-goto-registered-mark letter) (viper-getCom arg) nil)))))))

(require 'evil-common)


;; same as the evil-paste-before function in evil/evil-commands.el, except that i commented out the lines doing the backward-char
(evil-define-command evil-paste-before-no-backward-char
  (count &optional register yank-handler)
  "Pastes the latest yanked text before the cursor position.
The return value is the yanked text."
  :suppress-operator t
  (interactive "P<x>")
  (if (evil-visual-state-p)
      (evil-visual-paste count register)
    (evil-with-undo
      (let* ((text (if register
                       (evil-get-register register)
                     (current-kill 0)))
             (yank-handler (or yank-handler
                               (when (stringp text)
                                 (car-safe (get-text-property
                                            0 'yank-handler text)))))
             (opoint (point)))
        (when text
          (if (functionp yank-handler)
              (let ((evil-paste-count count)
                    ;; for non-interactive use
                    (this-command #'evil-paste-before))
                (push-mark opoint t)
                (insert-for-yank text))
            ;; no yank-handler, default
            (when (vectorp text)
              (setq text (evil-vector-to-string text)))
            (set-text-properties 0 (length text) nil text)
            (push-mark opoint t)
            (dotimes (i (or count 1))
              (insert-for-yank text))
            (setq evil-last-paste
                  (list #'evil-paste-before
                        count
                        opoint
                        opoint    ; beg
                        (point))) ; end
            (evil-set-marker ?\[ opoint)
            (evil-set-marker ?\] (1- (point)))
            ;;(when (> (length text) 0)
            ;;  (backward-char))
	    ))
        ;; no paste-pop after pasting from a register
        (when register
          (setq evil-last-paste nil))
        (and (> (length text) 0) text)))))

(setq evil-cross-lines 't)



(defun evilre-remap-colemak ()

  ;;(setq viper-insert-after-replace nil)
  ;;(setq viper-ESC-moves-cursor-back nil)
  ;;(setq viper-same-line-put-cursor-last-char nil)
  ;;(viper-set-searchstyle-toggling-macros 1) ;; unset '//' and '///' macros

  ;;(define-key evil-normal-state-map (kbd "<backspace>") 'vimpulse-visual-mode)
  ;;(define-key evil-normal-state-map (kbd "<delete>") 'delete-char)
  (define-key evil-normal-state-map (kbd "<return>") 'newline)

  ;;;;for some reason this gets overridden; put it in .emacs too
  (define-key evil-normal-state-map (kbd "<SPC>") 'evil-insert)



  ;;(define-key evil-normal-state-map (kbd "`") 'evil-paren-match)


  (define-key evil-normal-state-map (kbd "q") 'evil-open-below)

  ;;(define-key evil-normal-state-map (kbd "wg") 'vimpulse-goto-first-line)
  ;;(define-key evil-normal-state-map (kbd "wz") 'find-file-at-point)
  (define-key evil-normal-state-map "ww" 'ido-switch-buffer)
  ;;(define-key evil-normal-state-map "ww" 'helm-mini)
  ;;(define-key evil-normal-state-map "wj" 'jl-jump-backward)
  ;;(define-key evil-normal-state-map "wh" 'jl-jump-forward)
  ;;;;(define-key evil-normal-state-map "wq" 'quit-buffer)
  (define-key evil-normal-state-map "ws" 'save-buffer)
  ;;(define-key evil-normal-state-map "wa" 'evil-ex)
  ;;(define-key evil-normal-state-map "wf" 'find-file)
  ;;(define-key evil-normal-state-map "wb" 'bookmark-jump)
  ;;(define-key evil-normal-state-map "wB" 'bookmark-set)
  (define-key evil-normal-state-map "w1" 'delete-other-windows)
  (define-key evil-normal-state-map "wo" 'other-window) 
  ;;(define-key evil-normal-state-map "wg" 'pop-global-mark)

  ;;(define-key evil-normal-state-map "wu" 'viperre-quick-dvc-pull-update)
  ;;(define-key evil-normal-state-map "wc" 'viperre-quick-dvc-commit)
  ;;(define-key evil-normal-state-map "wv" 'dvc-log-edit)
  ;;(define-key evil-normal-state-map "wl" 'dvc-status)


  (define-key evil-motion-state-map (kbd "f") 'evil-backward-paragraph)
  (define-key evil-normal-state-map (kbd "f") nil)
  (define-key evil-visual-state-map (kbd "f") nil)
  ;; was evil-find-char
;;  ;;(define-key evil-normal-state-map (kbd "p") 'evil-scroll-down)
;;  ;;(define-key evil-normal-state-map (kbd "p") 'evil-search-backward-or-next)
  (define-key evil-motion-state-map (kbd "p") 'evil-search-backward)
  (define-key evil-normal-state-map (kbd "p") nil)
  (define-key evil-visual-state-map (kbd "p") nil)
  ;; was evil-paste-after

  ;;(;;define-key evil-normal-state-map (kbd "p") 'isearch-backward)

  ;; remap leader key: g -> y
  (define-key evil-normal-state-map (kbd "y") (lookup-key evil-normal-state-map (kbd "g")))
  (define-key evil-motion-state-map (kbd "y") (lookup-key evil-motion-state-map (kbd "g")))
  
  (define-key evil-normal-state-map (kbd "g") 'backward-kill-word)

  
  (define-key evil-normal-state-map (kbd "j") 'kill-word)
;;  ;;(define-key evil-normal-state-map (kbd "l") 'evil-search-forward-or-next)
  (define-key evil-motion-state-map (kbd "l") 'evil-search-forward)
  (define-key evil-normal-state-map (kbd "l") nil)
  (define-key evil-visual-state-map (kbd "l") nil)
;; ;;(define-key evil-normal-state-map (kbd "l") 'isearch-forward)
;;  ;;(define-key evil-normal-state-map (kbd "l") 'evil-scroll-up)
  (define-key evil-motion-state-map (kbd "u") 'evil-forward-paragraph)
  (define-key evil-normal-state-map (kbd "u") nil)
  (define-key evil-visual-state-map (kbd "u") nil)
  ;;  (define-key evil-normal-state-map (kbd "y") 'evil-change-command)
  ;;  (define-key evil-normal-state-map (kbd "Y") 'evil-change-to-eol)
    

  
  (define-key evil-normal-state-map (kbd ";") 'evil-open-above)
  ;; was evil-repeat-find-char
  (define-key evil-motion-state-map (kbd "a") 'evil-beginning-of-line)
  (define-key evil-normal-state-map (kbd "a") nil)
  (define-key evil-visual-state-map (kbd "a") nil)
  (define-key evil-motion-state-map (kbd "A") 'beginning-of-buffer)
  (define-key evil-normal-state-map (kbd "A") nil)
  (define-key evil-visual-state-map (kbd "A") nil)
  (define-key evil-motion-state-map (kbd "r") 'evil-backward-char)
  ;; was evil-replace
  (define-key evil-motion-state-map (kbd "s") 'evil-backward-word-begin)
  (define-key evil-motion-state-map (kbd "S") 'evil-backward-WORD-begin)
  (define-key evil-motion-state-map (kbd "t") 'evil-previous-line)

  (define-key evil-normal-state-map (kbd "r") nil)
  (define-key evil-normal-state-map (kbd "s") nil)
  (define-key evil-normal-state-map (kbd "S") nil)
  (define-key evil-normal-state-map (kbd "t") nil)

  (define-key evil-visual-state-map (kbd "r") nil)
  (define-key evil-visual-state-map (kbd "s") nil)
  (define-key evil-visual-state-map (kbd "S") nil)
  (define-key evil-visual-state-map (kbd "t") nil)
  
  ;;(define-key evil-normal-state-map (kbd "d") 'evil-find-char-backward-offset-or-next)
  ;;(define-key evil-normal-state-map (kbd "D") 'evil-find-char-backward-or-next)
  ;;(define-key evil-normal-state-map (kbd "h") 'evil-find-char-forward-offset-or-next)
  ;;(define-key evil-normal-state-map (kbd "H") 'evil-find-char-forward-or-next)
  (define-key evil-motion-state-map (kbd "n") 'evil-next-line)
  ;; was: evil-search-next
  (define-key evil-motion-state-map (kbd "e") 'evil-forward-word-begin)
  (define-key evil-motion-state-map (kbd "E") 'evil-forward-WORD-begin)
  (define-key evil-motion-state-map (kbd "i") 'evil-forward-char)
  (define-key evil-motion-state-map (kbd "o") 'evil-end-of-line)
  (define-key evil-motion-state-map (kbd "O") 'end-of-buffer)

  (define-key evil-normal-state-map (kbd "n") nil)
  (define-key evil-normal-state-map (kbd "e") nil)
  (define-key evil-normal-state-map (kbd "E") nil)
  (define-key evil-normal-state-map (kbd "i") nil)
  (define-key evil-normal-state-map (kbd "o") nil)
  (define-key evil-normal-state-map (kbd "O") nil)

 
  (define-key evil-visual-state-map (kbd "n") nil)
  (define-key evil-visual-state-map (kbd "e") nil)
  (define-key evil-visual-state-map (kbd "E") nil)
  (define-key evil-visual-state-map (kbd "i") nil)
  (define-key evil-visual-state-map (kbd "o") nil)
  (define-key evil-visual-state-map (kbd "O") nil)
  
  ;;(define-key evil-normal-state-map (kbd "'") 'evil-end-of-word)
  ;;(define-key evil-normal-state-map (kbd "\"") 'evil-end-of-WORD)


  ;; remap leader key: z -> '
  (define-key evil-normal-state-map (kbd "'") (lookup-key evil-motion-state-map (kbd "z")))
  (define-key evil-motion-state-map (kbd "'") (lookup-key evil-motion-state-map (kbd "z")))
  
  (define-key evil-normal-state-map (kbd "z") 'undo)
  (define-key evil-normal-state-map (kbd "Z") 'redo)
  ;; tombdo: do we want evil-save-modified-and-close somewhere?

  (define-key evil-normal-state-map (kbd "x") 'evil-delete)
  (define-key evil-normal-state-map (kbd "X") 'evil-delete-line)
  (define-key evil-normal-state-map (kbd "c") 'evil-yank)
  ;; was: evil-change
  (define-key evil-normal-state-map (kbd "C") 'evil-yank-line)
  (define-key evil-normal-state-map (kbd "v") 'evil-paste-before-no-backward-char)
  ;; was: evil-visual-char
  ;;(define-key evil-normal-state-map (kbd "V") 'evil-paste-before)
  (define-key evil-normal-state-map (kbd "b") 'backward-delete-char-untabify)
  (define-key evil-normal-state-map (kbd "k") 'delete-char)

  ;; m is now evil-set-marker (` is goto mark')
  ;;(define-key evil-normal-state-map (kbd "m") (evilre-construct-viper-goto-registered-mark-keybindings))
  ;;(define-key evil-normal-state-map (kbd "m SPC") 'set-mark-command)
  ;;(define-key evil-normal-state-map (kbd "mm") 'evil-goto-mark)
  ;;(define-key evil-normal-state-map (kbd "m,") 'set-mark-command-deactivate)
  ;;(define-key evil-normal-state-map (kbd "m.") 'exchange-point-and-mark-deactivate)
  ;;(define-key evil-normal-state-map (kbd "m/") 'evil-cycle-through-mark-ring)
  ;;;;(define-key evil-normal-state-map (kbd "mA") 'evil-mark-beginning-of-buffer)
  ;;;;(define-key evil-normal-state-map (kbd "mO") 'evil-mark-end-of-buffer) 
  ;;(define-key evil-normal-state-map (kbd "m^") (lambda () (interactive) (push-mark viper-saved-mark t t)))
  ;;(define-key evil-normal-state-map (kbd "mD") (lambda () (interactive) (mark-defun)))

  ;;(define-key evil-normal-state-map (kbd ",") 'evil-substitute)
  ;; , is a leader key in spacemacs
  (define-key evil-normal-state-map (kbd ".") 'evil-specify-register) ;; todo
  ;;(define-key evil-normal-state-map (kbd "/") 'evil-repeat)
  (define-key evil-normal-state-map (kbd "/") 'evil-record-macro)
  
  (define-key evil-normal-state-map (kbd "\\") 'evil-execute-in-emacs-state)

  (define-key evil-normal-state-map (kbd "!") 'evil-shell-command)
  ;;;;(define-key evil-normal-state-map (kbd "") 'evil-hash-command)
  (define-key evil-normal-state-map (kbd "=") 'evil-indent)
  ;; todo: what was (viper?)-equals-command in viper?
  (define-key evil-normal-state-map (kbd "<") 'evil-shift-left)
  (define-key evil-normal-state-map (kbd ">") 'evil-shift-right)

  (define-key evil-insert-state-map (kbd "C-w") 'close-current-buffer)
  (define-key evil-motion-state-map (kbd "C-w") 'close-current-buffer)
  (define-key evil-insert-state-map (kbd "C-o") nil)
  (define-key evil-motion-state-map (kbd "C-o") nil)
  (define-key evil-visual-state-map (kbd "C-o") nil)


  (define-key evil-normal-state-map (kbd "Q") 'query-replace)

  ;;(define-key evil-motion-state-map (kbd "TAB") nil)
  ;; why did i have that there?


  ;;;; (keymap
  ;;;;  (22 . vimpulse-visual-mode-block)
  ;;;;  (16 . yank-rectangle)
  ;;;;  (23 keymap
  ;;;;      (83 . split-window-vertically)
  ;;;;      (115 . split-window-vertically)
  ;;;;      (99 . delete-window)
  ;;;;      (111 . delete-other-windows)
  ;;;;      (119 . vimpulse-cycle-windows)
  ;;;;      (23 . vimpulse-cycle-windows))
  ;;;;  (18 . redo)
  ;;;;  (117 . undo)
  ;;;;  (20 . pop-tag-mark)
  ;;;;  (29 . vimpulse-jump-to-tag-at-point)
  ;;;;  (35 . vimpulse-search-backward-for-symbol-at-point)
  ;;;;  (42 . vimpulse-search-forward-for-symbol-at-point)
  ;;;;  (122 keymap
  ;;;;       (122 . viper-line-to-middle)
  ;;;;       (116 . viper-line-to-top)
  ;;;;       (108 . scroll-left)
  ;;;;       (104 . scroll-right)
  ;;;;       (98 . viper-line-to-bottom))
  ;;;;  (75 . woman)
  ;;;;  (27 . newline)
  ;;;;  (32 . insert-space)
  ;;;;  (return . newline)
  ;;;;  (delete . delete-char)
  ;;;;  (backspace . backward-delete-char-untabify)
  ;;;;  (25 . yank)

  ;;(viper-init-keymap)
  ;;(add-to-list 'evil-movement-commands (viper-where-is-internal 'evil-search-backward-or-next))
  ;;(add-to-list 'evil-movement-commands (viper-where-is-internal 'evil-find-char-forward-offset-or-next))
  ;;(add-to-list 'evil-movement-commands (viper-where-is-internal 'evil-find-char-backward-offset-or-next))
  ;;(add-to-list 'evil-movement-commands (viper-where-is-internal 'evil-find-char-forward-or-next))
  ;;(add-to-list 'evil-movement-commands (viper-where-is-internal 'evil-find-char-backward-or-next))
)












;; BEGIN from ergoemacs-keybindings, take this out and just use a spacemacs layer if desired

;; note: emacs won't offer to save a buffer that's
;; not associated with a file,
;; even if buffer-modified-p is true.
;; One work around is to define your own my-kill-buffer function
;; that wraps around kill-buffer, and check on the buffer modification
;; status to offer save
;; This custome kill buffer is close-current-buffer.


(defvar recently-closed-buffers (cons nil nil) "A list of recently closed buffers. The max number to track is controlled by the variable recently-closed-buffers-max.")
(defvar recently-closed-buffers-max 10 "The maximum length for recently-closed-buffers.")

(defun close-current-buffer ()
"Close the current buffer.

Similar to (kill-buffer (current-buffer)) with the following addition:

• prompt user to save if the buffer has been modified even if the buffer is not associated with a file.
• make sure the buffer shown after closing is a user buffer.
• if the buffer is a file, add the path to the list recently-closed-buffers.

A emacs buffer is one who's name starts with *.
Else it is a user buffer."
 (interactive)
 (let (emacsBuff-p isEmacsBufferAfter)
   (if (string-match "^*" (buffer-name))
       (setq emacsBuff-p t)
     (setq emacsBuff-p nil))

   ;; offer to save buffers that are non-empty and modified, even for non-file visiting buffer. (because kill-buffer does not offer to save buffers that are not associated with files)
   (when (and (buffer-modified-p)
              (not emacsBuff-p)
              (not (string-equal major-mode "dired-mode"))
              (if (equal (buffer-file-name) nil) 
                  (if (string-equal "" (save-restriction (widen) (buffer-string))) nil t)
                t
                )
              )
     (if (y-or-n-p
            (concat "Buffer " (buffer-name) " modified; Do you want to save?"))
       (save-buffer)
       (set-buffer-modified-p nil)))

   ;; save to a list of closed buffer
   (when (not (equal buffer-file-name nil))
     (setq recently-closed-buffers
           (cons (cons (buffer-name) (buffer-file-name)) recently-closed-buffers))
     (when (> (length recently-closed-buffers) recently-closed-buffers-max)
           (setq recently-closed-buffers (butlast recently-closed-buffers 1))
           )
     )

   ;; close
   (kill-buffer (current-buffer))

   ;; if emacs buffer, switch to a user buffer
   (if (string-match "^*" (buffer-name))
       (setq isEmacsBufferAfter t)
     (setq isEmacsBufferAfter nil))
   (when isEmacsBufferAfter
     (previous-user-buffer)
     )
   )
 )

;; END from ergoemacs-keybindings


(provide 'evilre)
